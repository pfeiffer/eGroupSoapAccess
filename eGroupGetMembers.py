#!/usr/bin/env python3

#
#  Updated version to access the CERN eGroup SOAP interface using Zeep:
#     Zeep: http://docs.python-zeep.org/en/master/index.html
#
#     eGroups: https://aistools-prod.cern.ch/docs/display/ED/E-groups+Documentation
#     SOAP interface: https://aistools-prod.cern.ch/docs/display/ED/6.+Web+Service
#
# last update: AP, 2017-08-11
# 

import os, sys

import zeep
import netrc

from zeep.transports import Transport
from zeep.plugins import HistoryPlugin

from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.


# import subprocess
# import copy
# import time

def getGroupMembers( eGroupName ):

    url = 'https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl'

    # get the credentials from your ~/.netrc file:
    auth = netrc.netrc()
    login, acct, pwd = auth.authenticators('egroupsap.cern.ch')  # use whatever machine name you have set up

    session = Session()
    session.auth = HTTPBasicAuth(login, pwd)

    history = HistoryPlugin()
    client = zeep.Client(url, 
                         strict = False, 
                         transport = Transport(session=session),
                         plugins=[history] )

    groupInfo = client.service.FindEgroupByName(groupName)

    # to see the response, write it to a file:
    # with open('foobar.txt','w') as lf:
    #     lf.write( str(groupInfo) )

    memberList = []
    for member in groupInfo.result.Members :

        # ignore other (static) eGroups
        if member.Type == 'StaticEgroup' : continue

        try:
            memInfo = member.PrimaryAccount if member.PrimaryAccount else '---'
            memInfo += ';' + (str(member.ID) if member.ID is not None else '???')
            if member.Email: memInfo += ';'+member.Email
            memberList.append( memInfo )
        except AttributeError:
            print( "No account ??? for: %s" % str(member) )
            continue

    return memberList

if __name__ == "__main__":

    groupName = sys.argv[1].lower() if len(sys.argv)>1 else 'zh'

    print( 'getting members for eGroup %s' % groupName )

    memList = getGroupMembers(groupName)
    noPA = [ x for x in memList if x.startswith('---') ]
    noID = [ x for x in memList if '???' in x ]

    print( 'found %d members' % len(memList) )
    print( 'no PA: %d, no ID: %d ' % (len(noPA), len(noID)) )
#     print( 'members[:100]: %s' % str(memList[:100]) )
