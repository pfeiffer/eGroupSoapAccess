
# Accessing the eGroup SOAP Web Services using Python and Zeep

Updated version to access the CERN eGroup SOAP interface using Zeep. 

References:

 -  Zeep: http://docs.python-zeep.org/en/master/index.html
 -  eGroups: https://aistools-prod.cern.ch/docs/display/ED/E-groups+Documentation
 -  SOAP interface: https://aistools-prod.cern.ch/docs/display/ED/6.+Web+Service

## Setting up the (virtual) environment


Using venv with python3 on CC-7 gave issues, which were resolved by:

	sudo pip install --upgrade virtualenv
	
before creating the venv:
	
	virtualenv --python=python3 venv
	
then, adding Zeep worked via:
	
	(source ./venv/bin/activate; pip install zeep )
	
